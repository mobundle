#!/bin/sh
md="$(dirname "$(readlink -f "$0")")"
PERL5LIB="$md/lib:$md/local/lib/perl5" "$md/mobundle" \
   -LPo "$md/bundle/mobundle" \
   -m App::MoBundle \
   -m Module::ScanDeps \
   -m Module::ScanDeps::Cache \
   -m Template::Perlish \
   "$md/mobundle"
chmod +x "$md/bundle/mobundle"
