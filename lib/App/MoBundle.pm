#!/usr/bin/env perl
use strict;
use warnings;
our $VERSION = '0.2.0';

use Carp;
use English qw< -no_match_vars >;
use File::Basename qw< basename dirname >;
use File::Path qw< make_path >; # "mkdir -p"-alike
use File::Spec;
use File::Spec::Unix;
use Cwd qw< realpath >;

use Exporter qw< import >;
our @EXPORT_OK = qw<
   autoscan
   bundle
   mobundle_config
   mobundle_run
   unbundle
>;
our %EXPORT_TAGS = (all => [@EXPORT_OK]);

########################################################################
#
# Exported functions

sub autoscan {
   my ($config) = @_;
   get_modules($config);
   add_autoscan_modules($config); # assume autoscan is requested!
   return $config->{modules};
}

sub bundle {
   my ($config) = @_;
   get_modules($config);

   # conditionally scan modules too
   add_autoscan_modules($config) if $config->{autoscan};

   my $template = <<'END_OF_TEMPLATE';
[% head %]

# __MOBUNDLE_INCLUSION__
BEGIN {
   my %file_for = (
[% while (my ($filename, $contents) = each %{$variables{modules}}) { %]
      '[%= $filename %]' => <<'END_OF_FILE',
[%= $contents =~ s/^/ /gmxs; $contents; %]
END_OF_FILE
[% } %]
   );

   unshift @INC, sub {
      my ($me, $packfile) = @_;
      return unless exists $file_for{$packfile};
      (my $text = $file_for{$packfile}) =~ s/^\ //gmxs;
      chop($text); # added \n at the end
      open my $fh, '<', \$text or die "open(): $!\n";
      return $fh;
   };

[% if ($variables{'add-modules-list'}) { %]
   our @__MOBUNDLE_MODULES__ = qw<
[%    for my $path (sort {$a cmp $b} keys(%{$variables{modules}})) {
         (my $name = $path) =~ s{\.pm$}{}mxs or next;
         $name =~ s{/}{::}gmxs;
%]      [%= $name %]
[%    }
%]   >;
[% } %]
} ## end BEGIN
# __MOBUNDLE_INCLUSION__

[% body %]
END_OF_TEMPLATE

   require Template::Perlish;
   return Template::Perlish::render($template, $config);
}

sub mobundle_config {
   my ($config, $args) = @_;

   if (defined($config->{'standard-head'})) {
      die(\'head and standard-head are mutually exclusive')
         if defined($config->{head});
      $config->{head} = "#!/usr/bin/env perl\n";
   }

   if (defined($config->{'head-from'})) {
      die(\'multiple head sources are not allowed')
         if defined($config->{head});
      $config->{head} = read_file($config->{'head-from'});
   }

   # Get body
   if (@{$args}) {
      die(\'body and bare parameter are mutually exclusive')
        if defined($config->{body});
      die(\'body-from and bare parameter are mutually exclusive')
        if defined($config->{'body-from'});
      die(\'only one bare command line parameter is allowed')
        if @{$args} > 1;
      $config->{'body-from'} = $args->[0];
   }

   if (defined($config->{'body-from'})) {
      die(\'body and body-from are mutually exclusive')
         if defined($config->{body});
      $config->{body} = read_file($config->{'body-from'});
   }

   die(\'neither body nor body-from were present')
     unless defined($config->{body});

   if (exists($config->{'head-from-body'})) {
      die(\'multiple head sources are not allowed')
        if defined($config->{head});

      my @body = split /\n/, $config->{body};
      my @header = splice @body, 0, $config->{'head-from-body'} || 1;

      $config->{head} = join "\n", @header;
      $config->{body} = join "\n", @body;
   } ## end if (exists $config->{'head-from-body'...

   if (exists($config->{'head-from-paragraph'})) {
      die(\'multiple head sources are not allowed')
        if exists($config->{head});

      ($config->{head}, $config->{body}) = split /\n\s*?\n/, $config->{body}, 2;
   }

   $config->{modules} = [] unless defined($config->{modules});
   for my $file (@{$config->{'modules-from'}}) {
      chomp(my @modules = read_file($file));
      push @{$config->{modules}}, @modules;
   }

   return $config;
}

sub mobundle_run {
   my ($prg, @args) = @_;

   require Pod::Usage;
   require Getopt::Long;
   Getopt::Long::Configure('gnu_getopt');

   my %config = (output => '-', 'modules-from' => [], include => []);
   Getopt::Long::GetOptionsFromArray(
      \@args,
      \%config,
      qw(
      usage help man version
      add-modules-list|L!
      autoscan|scan|a!
      autoscan-list|scan-list|modules-list|l!
      body|b=s
      body-from|script|program|B=s
      head|h=s
      head-from|H=s
      head-from-body|S:i
      head-from-paragraph|P!
      include|I=s@
      modules|module|m=s@
      modules-from|M=s@
      output|o=s
      standard-head|s!
      unbundle|u!
      )
   );
   Pod::Usage::pod2usage(message => "$prg $VERSION", -verbose => 99, -sections => ' ')
     if $config{version};
   Pod::Usage::pod2usage(-verbose => 99, -sections => 'USAGE') if $config{usage};
   Pod::Usage::pod2usage(-verbose => 99, -sections => 'USAGE|EXAMPLES|OPTIONS')
     if $config{help};
   Pod::Usage::pod2usage(-verbose => 2) if $config{man};

   # Manage unbundle before all the rest
   return unbundle(\%config, @args) if $config{unbundle};

   # "Unroll" the configuration to cope with options like body-from and
   # head-from-paragraph... This might lead to validation breaking and some
   # help to the user
   eval {
      mobundle_config(\%config, \@args);
      1;
   } or do {
      my $message = $EVAL_ERROR;
      $message = ${$message} if ref($message) eq 'SCALAR';
      Pod::Usage::pod2usage(
         message   => "ERROR: $message\n",
         -verbose  => 99,
         -sections => ''
      );
   };

   if ($config{'autoscan-list'}) {
      my $modules = autoscan(\%config);
      for my $path (sort { $a cmp $b } keys(%{$modules})) {
         (my $name = $path) =~ s/\.pm$//;
         $name =~ s{/}{::}g;
         print "$name\n";
      }
      return 0;
   }

   write_file($config{output}, bundle(\%config));
   return 0;

}

sub unbundle {
   my $config = shift;

   BUNDLED:
   for my $bundled (@_) {
      my $modules = read_modules($bundled) or next BUNDLED;
      while (my ($path, $contents) = each(%{$modules})) {
         my $output = $config->{output} ne '-' ? $config->{output} : 'lib';
         my $path = unix_to_local_path("$output/$path");
         make_path(dirname($path)); # ensure parent directory
         write_file($path, $contents);
      }
   }

   return 0;
}

########################################################################
#
# Helpers

sub add_autoscan_modules {
   my ($config) = @_;
   $config->{modules} = {} unless defined($config->{modules});
   my $modules = $config->{modules};

   require Module::ScanDeps;
   require File::Temp;
   require Config;

   my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.pl');
   write_file($fh, $config->{body});
   $fh->close();

   my $in_priv = subsumer_factory($Config::Config{privlib});
   my $in_arch = subsumer_factory($Config::Config{archlib});

   my @filenames = $fh->filename;
   my %flag_for;
   while (@filenames) {
      my $name = shift @filenames;
      next if $flag_for{$name}++;
      my $deps_for =
        Module::ScanDeps::scan_deps(files => [$name], skip => $modules);

      while (my ($key, $mod) = each(%{$deps_for})) {
         next if exists $modules->{$key};

         # Restrict to modules...
         next unless $mod->{type} eq 'module';

         my $filename = $mod->{file};
         next if $in_priv->($filename) || $in_arch->($filename);

         $modules->{$key} = read_file($filename);
         push @filenames, $filename;
      } ## end while (my ($key, $mod) = ...
   }
   return $config;
}

sub get_module_contents {
   my ($filename) = @_;
   for my $item (@INC) {
      my $full_path = unix_to_local_path("$item/$filename");
      next unless -e $full_path;
      return scalar(read_file($full_path));
   } ## end for my $item (@INC)
   carp "could not find module file: '$filename'";
} ## end sub get_module_contents

sub get_modules {
   my ($config) = @_;

   # widen search path for modules, but only until we exit from this sub
   local @INC = @INC;
   push @INC, @{$config->{include} || []};

   my %modules = map {
      (my $filename = $_) =~ s{::}{/}g;
      $filename .= '.pm' unless $filename =~ /\./mxs;

      # "return" the pair
      ($filename => get_module_contents($filename));
   } @{$config->{modules}};

   $config->{modules} = \%modules;
   return $config;
}

sub fh_raw {
   my ($file, $mode) = @_;
   $file = $mode eq '<' ? \*STDIN : \*STDOUT
      if (! ref($file)) && ($file eq '-');
   my ($fh, $opened);

   # if $file is a reference, but NOT a reference to a SCALAR, assume it
   # already supports I/O operations and take it as-is. Otherwise, open
   # it (this includes references to scalars for in-memory stuff)
   my $ref = ref($file);
   if ($ref && $ref ne 'SCALAR') {
      $fh = $file;
   }
   else {
      open $fh, $mode, $file or croak "open('$file'): $OS_ERROR";
      $opened = 1;
   }
   binmode $fh, ':raw' or croak "binmode(): $OS_ERROR";
   return ($fh, $opened);
}

sub read_file {
   my ($fh, $opened) = fh_raw($_[0], '<');

   local $INPUT_RECORD_SEPARATOR = $INPUT_RECORD_SEPARATOR;
   $INPUT_RECORD_SEPARATOR = undef unless wantarray();
   my @retval = readline($fh) or croak "readline(): $OS_ERROR";

   if ($opened) {
      close($fh) or croak "close(): $OS_ERROR";
   }

   return wantarray() ? @retval : $retval[0];
}

sub read_modules {
   my ($bundled) = @_;

   my ($fh, $opened) = fh_raw($bundled, '<');

   # read __MOBUNDLE_INCLUSION__ section
   my @lines;
   1 while scalar(<$fh>) !~ m{^\#\ __MOBUNDLE_INCLUSION__$}mxs;
   while (<$fh>) {
      last if m{^\#\ __MOBUNDLE_INCLUSION__$}mxs;
      push @lines, $_;
   }

   # read no more
   if ($opened) {
      close($fh) or croak "close(): $OS_ERROR";
   }

   if (!@lines) {
      warn "nothing in $bundled\n";
      return;
   }

   1 while shift(@lines) !~ m{^\s*my \s* \%file_for}mxs;
   unshift @lines, '(';
   1 while pop(@lines) !~ m{^\s*unshift \s* \@INC}mxs;
   my $definition = join '', @lines;

   my %file_for = eval $definition;
   return wantarray() ? %file_for : \%file_for;
}


sub subsumer_factory {
   my ($dirpath) = @_;
   $dirpath = realpath($dirpath) if defined($dirpath) && length($dirpath);

   return sub { return 0 } unless defined($dirpath) && length($dirpath);

   my ($dv, $dds) = File::Spec->splitpath($dirpath, 'no-file');
   my @dds = File::Spec->splitdir($dds);

   return sub {
      my $filepath = realpath($_[0]);
      my ($fv, $fds, $fbase) = File::Spec->splitpath($filepath);

      return 0 if $dv ne $fv;                  # different volume?

      my @fds = File::Spec->splitdir($fds);
      # warn "- $filepath\n    fv<$fv> fds(@fds)\n    dv<$dv> dds(@dds)\n";
      return 0 if scalar(@dds) > scalar(@fds); # not long enough?
      for my $i (0 .. $#dds) {
         return 0 if $dds[$i] ne $fds[$i];     # not same (sub)-dir
      }

      # everything matched...
      return 1;
   };
}

sub unix_to_local_path {
   my ($upath, $is_dir) = @_;
   $upath = File::Spec::Unix->canonpath($upath); # remove multiple '/'...
   my ($vol, $dirs, $file) = File::Spec::Unix->splitpath($upath, $is_dir);
   $dirs = File::Spec->catdir(File::Spec::Unix->splitdir($dirs));
   return File::Spec->catpath($vol, $dirs, $file);
}

sub write_file {
   my ($fh, $opened) = fh_raw(shift, '>');

   print {$fh} @_ or croak "print(): $OS_ERROR";

   if ($opened) {
      close($fh) or croak "close(): $OS_ERROR";
   }

   return;
}

1;
